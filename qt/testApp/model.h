#ifndef MODEL_H
#define MODEL_H

#include <QStringList>
#include <QStandardItemModel>
#include "speach2textPresenter.h"

class QMLModel : public QObject
{
    Q_OBJECT
public:

    Q_PROPERTY(int SourceIndex WRITE setSource READ getSource NOTIFY SourceChanged)
    Q_PROPERTY(QStringList Sources READ getSources NOTIFY SourcesChanged)
    Q_PROPERTY(int LanguageIndex WRITE setLanguage READ getLanguage NOTIFY LanguageChanged)
    Q_PROPERTY(QStringList Languages READ getLanguages NOTIFY LanguagesChanged)
    Q_PROPERTY(bool Capture READ getCapture WRITE setCapture NOTIFY CaptureChanged)
    Q_PROPERTY(QString Text READ getText NOTIFY TextChanged)
    Q_PROPERTY(QString Log READ getLog NOTIFY LogChanged)


    QMLModel(){
        connect(&speach2text_, SIGNAL(newWord(const QString &)), this, SLOT(newWord(const QString &)));
        connect(&speach2text_, SIGNAL(newState(const QString &)), this, SLOT(newState(const QString &)));
    }

public slots:

    const QString getText()
    {
        return text_;
    }

    const QString getLog()
    {
        return log_;
    }

    const QStringList getSources()
    {
        return speach2text_.getSources();
    }

    int getSource(){
        return sourceIndex_;
    }

    void setSource(const int &  index){
        sourceIndex_ = index;
        speach2text_.setSource(index);
        SourceChanged();
    }

    const QStringList getLanguages()
    {
        return speach2text_.getLanguages();
    }

    int getLanguage(){
        return languageIndex_;
    }

    void setLanguage(const int & index){
        languageIndex_ = index;
        speach2text_.setLanguage(index);
        LanguageChanged();
    }

    bool getCapture()const{
        return speach2text_.IsCaptureActive();
    }

    void setCapture(const bool &capture){
        if (capture){
            text_ = "";
            log_ = "";
            TextChanged();
            LogChanged();
            speach2text_.startCapture();
        }else
            speach2text_.stopCapture();
        CaptureChanged();
    }

private slots:
    void newWord(const QString& word)
    {
        text_.append(word);
        text_.append(" ");
        TextChanged();
    }

    void newState(const QString& state)
    {
        log_.append(state);
        log_.append("\n");
        LogChanged();
    }

signals:
    void CaptureChanged();
    void SourcesChanged();
    void LanguagesChanged();
    void SourceChanged();
    void LanguageChanged();
    void TextChanged();
    void LogChanged();

private:
    int sourceIndex_ = -1, languageIndex_ = -1;
    QString text_, log_;
    Speach2TextPresenter speach2text_;
};

#endif // MODEL_H
