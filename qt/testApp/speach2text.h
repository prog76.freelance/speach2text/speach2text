#ifndef SPEACH2TEXT_H
#define SPEACH2TEXT_H

#include <filesystem>
#include <functional>

#include <sphinxbase/err.h>
#include <sphinxbase/ad.h>

#include "pocketsphinx.h"

using namespace std::experimental;

class Speach2Text{

public:

    Speach2Text(std::function<void(const char*)> newState):
      newState_(newState)
    { }

    void prepare(const std::string & source, const std::string & language){
        cmd_ln_t* config;
        if(filesystem::exists("model.params"))
            config = cmd_ln_parse_file_r(NULL, psphinxDef, "model.params", FALSE);
        else{
            filesystem::path folder(filesystem::current_path());
            filesystem::path hmm(folder), lm(folder), dict(folder);
            hmm.append(language);
            lm.append(language+".lm.bin");
            dict.append(std::string("cmudict-")+language+".dict");
            config = cmd_ln_init(NULL, psphinxDef, FALSE, "-hmm", hmm.u8string().c_str(), "-lm", lm.u8string().c_str(), "-dict", dict.u8string().c_str(), NULL);
        }

        ps_default_search_args(config);
        ps_ = ps_init(config);
        if (ps_ == NULL)
            throw std::exception("Failed to create decoder");
        auto rate = (int) cmd_ln_float32_r(config, "-samprate");
        if ((ad_ = ad_open_dev(source.c_str(), rate)) == NULL)
            throw std::exception("Failed to open audio device");

        if (ad_start_rec(ad_) < 0)
            throw std::exception("Failed to start recording\n");

        if (ps_start_utt(ps_) < 0)
            throw std::exception("Failed to start utterance\n");

        utt_started_ = FALSE;
    }

    const char* iterate(){
        int k;
        if ((k = ad_read(ad_, adbuf_, sizeof(adbuf_)/sizeof(adbuf_[0]))) < 0)
            E_FATAL("Failed to read audio\n");
        if (k>0){
            ps_process_raw(ps_, adbuf_, k, FALSE, FALSE);
            in_speech_ = ps_get_in_speech(ps_);
            if (in_speech_ && !utt_started_) {
                utt_started_ = TRUE;
                E_INFO("Listening...\n");
                newState_("Listening...");
            }
            if (!in_speech_ && utt_started_) {
                /* speech -> silence transition, time to start new utterance  */
                ps_end_utt(ps_);
                word_ = ps_get_hyp(ps_, NULL );
                if (ps_start_utt(ps_) < 0)
                    E_FATAL("Failed to start utterance\n");
                utt_started_ = FALSE;
                E_INFO("Ready....\n");
                newState_("Ready...");
                return word_.c_str();
            }
            return NULL;
        }
    }

    void stop(){
        ad_close(ad_);
    }

private:
    static constexpr arg_t psphinxDef[] = {POCKETSPHINX_OPTIONS, CMDLN_EMPTY_OPTION};
    std::string word_;
    std::function<void(const char*)> newState_;
    int16 adbuf_[2048];
    uint8 utt_started_, in_speech_;
    ad_rec_t *ad_;
    ps_decoder_t *ps_;
};

#endif // SPEACH2TEXT_H

