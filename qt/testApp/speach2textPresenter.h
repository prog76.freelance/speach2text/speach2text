#ifndef SPEACH2TEXTP_H
#define SPEACH2TEXTP_H

#include <QAudioRecorder>
#include <QThread>
#include <QWaitCondition>
#include <QMutex>
#include <QString>
#include "speach2text.h"

class Speach2TextPresenter: public QThread {
    Q_OBJECT
public:
    Speach2TextPresenter():
        speach2Text_(
            [this](const char* state){
        emit newState(QString::fromUtf8(state));
    }
    ) {
        initLanguages();
    }

    ~Speach2TextPresenter(){
        exit_ = true;
        armThread();
        wait();
    }

    void setSource(int index){
        source_ = std::to_string(index);
    }

    QStringList getSources(){
        auto list = recorder_.audioInputs();
        return list;
    }

    void setLanguage(int index){
        language_ = getLanguages()[index].toUtf8().constData();
    }

    QStringList getLanguages(){
        return languages_;
    }

    void startCapture(){
        if(!isRunning())
            start(QThread::TimeCriticalPriority);
        emit newState("Initializing...");
        filesystem::current_path(app_path_);
        filesystem::current_path(filesystem::current_path().append("model").append(language_));
        speach2Text_.prepare(source_.c_str(), language_);
        emit newState("Capture ready");
        armThread();
    }

    void stopCapture(){
        active_ = false;

    }

    bool IsCaptureActive()const{
        return active_;
    }

    void run() override
    {
        while (!exit_) {
            {
                mutex_.lock();
                if (!active_)
                    start_.wait(&mutex_);
                mutex_.unlock();
                emit newState("Enter capture loop");
                while(active_ && !exit_)
                {
                    const char* word = speach2Text_.iterate();
                    if(word == NULL)
                        continue;
                    emit newWord(QString::fromUtf8(word));
                    msleep(100);
                }
                emit newState("Capture loop done");
                speach2Text_.stop();
            }
        }
    }

signals:
    void newWord(const QString &);
    void newState(const QString &);

private:

    void initLanguages(){
        languages_.clear();
        app_path_ = filesystem::current_path();
        for(auto& p: filesystem::directory_iterator(filesystem::current_path().append("model")))
            if(p.status().type() == filesystem::file_type::directory)
                languages_.append(QString::fromUtf8(p.path().filename().generic_u8string().c_str()));
    }


    void armThread(){
        mutex_.lock();
        active_ = true;
        start_.wakeAll();
        mutex_.unlock();
    }

    filesystem::path app_path_;
    QStringList languages_;
    std::string language_;
    std::string source_;
    Speach2Text speach2Text_;
    bool active_ = false;
    QWaitCondition start_;
    QMutex mutex_;
    bool exit_ = false;
    QAudioRecorder recorder_;
};

#endif // SPEACH2TEXTP_H
