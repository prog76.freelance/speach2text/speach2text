#TEMPLATE=vcapp

QT += quick
QT += multimedia
CONFIG += c++11

QMAKE_EXTRA_TARGETS += before_build makefilehook

makefilehook.target = $(MAKEFILE)
makefilehook.depends = .beforebuild

PRE_TARGETDEPS += .beforebuild

before_build.target = .beforebuild
before_build.depends = FORCE
before_build.commands = chcp 1251

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

VS = $$absolute_path($$PWD/../../vs/)

INCLUDEPATH += $${VS}/sphinxbase/include
INCLUDEPATH += $${VS}/sphinxbase/include/win32
INCLUDEPATH += $${VS}/pocketsphinx/include

PSPHINX = $${VS}/pocketsphinx/bin/
SPHINX = $${VS}/sphinxbase/bin/

CONFIG(release, debug|release): {
  PSPHINX = $${PSPHINX}Release/
  SPHINX = $${SPHINX}Release/
  DESTDIR = $$OUT_PWD/release/
}
else: CONFIG(debug, debug|release): {
  PSPHINX = $${PSPHINX}Debug/
  SPHINX = $${SPHINX}Debug/
  DESTDIR = $$OUT_PWD/debug/
}

!contains(QMAKE_TARGET.arch, x86_64) {
  PSPHINX = $${PSPHINX}Win32/
  SPHINX = $${SPHINX}Win32/
} else {
  PSPHINX = $${PSPHINX}x64/
  SPHINX = $${SPHINX}x64/
}

win32 {
  DESTDIR ~= s,/,\\,g
  PSPHINX ~= s,/,\\,g
  SPHINX ~= s,/,\\,g
  VS ~= s,/,\\,g
  #system(copy $${PSPHINX}*.dll $${DESTDIR})
  #system(copy $${PSPHINX}*.pdb $${DESTDIR})
  #system(copy $${SPHINX}*.dll $${DESTDIR})
  #system(copy $${SPHINX}*.pdb $${DESTDIR})
  #system(xcopy $${VS}\pocketsphinx\model $${DESTDIR}model\ /s /y /q)
  system(mklink /H $${DESTDIR}pocketsphinx.dll $${PSPHINX}pocketsphinx.dll)
  system(mklink /H $${DESTDIR}pocketsphinx.pdb $${PSPHINX}pocketsphinx.pdb)
  system(mklink /H $${DESTDIR}sphinxbase.dll $${SPHINX}sphinxbase.dll)
  system(mklink /H $${DESTDIR}sphinxbase.pdb $${SPHINX}sphinxbase.pdb)
  system(mklink /J $${DESTDIR}model $${VS}\pocketsphinx\model)
}

LIBS += $${PSPHINX}pocketsphinx.lib $${SPHINX}sphinxbase.lib

#QMAKE_CXXFLAGS_RELEASE += /Zi
#QMAKE_CXXFLAGS_RELEASE += /Od

#QMAKE_LFLAGS_RELEASE += /DEBUG
#QMAKE_LFLAGS_RELEASE += /PDB:"release\testApp.pdb"

HEADERS += \
    model.h \
    speach2textPresenter.h \
    speach2text.h
