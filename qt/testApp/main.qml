import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QtQml.Models 2.3

Window {
    visible: true
    title: qsTr("Speach to text")
    property int margin: 11
    width: 750
    height: 300
    RowLayout{
        anchors.fill: parent
        anchors.margins: margin
        ColumnLayout{
            Layout.fillWidth: true
            anchors.margins: margin
            Layout.minimumWidth: 350
            GroupBox {
                Layout.fillWidth: true
                title: "Recording source"
                ComboBox {
                    anchors.fill: parent
                    model: Model.Sources
                    enabled: !Model.Capture
                    currentIndex: Model.SourceIndex
                    onActivated: {
                        Model.SourceIndex = currentIndex
                    }

                    onCurrentIndexChanged: {
                        Model.SourceIndex = currentIndex
                    }
                }
            }
            GroupBox {
                Layout.fillHeight: true
                Layout.fillWidth: true
                title: "Results"
                ScrollView {
                    id: scrollView
                    anchors.fill: parent
                    TextArea{
                        text: Model.Text
                        wrapMode: "Wrap"
                    }
                }
            }
        }
        ColumnLayout{
            anchors.margins: margin
            Layout.minimumWidth: 150
            RowLayout{
                Layout.fillWidth: true
                anchors.margins: margin
                GroupBox {
                    Layout.fillWidth: true
                    title: "Language"
                    ComboBox {
                        anchors.fill: parent
                        model: Model.Languages
                        enabled: !Model.Capture
                        currentIndex: Model.LanguageIndex
                        onActivated: {
                            Model.LanguageIndex = currentIndex
                        }
                        onCurrentIndexChanged: {
                            Model.LanguageIndex = currentIndex
                        }
                    }
                }
                GroupBox {
                    title: "Capture"
                    CheckBox {
                        enabled: (Model.LanguageIndex!=-1) && (Model.SourceIndex!=-1)
                        checked: Model.Capture
                        onClicked: {
                            Model.Capture = checked;
                            checked = Qt.binding(function () { // restore the binding
                                return Model.Capture;
                            });
                        }
                    }
                }
            }
            GroupBox {
                Layout.fillHeight: true
                Layout.fillWidth: true
                title: "Log"
                ScrollView {
                    id: scrollView1
                    anchors.fill: parent
                    TextArea{
                        text: Model.Log
                    }
                }
            }
        }
    }

}
